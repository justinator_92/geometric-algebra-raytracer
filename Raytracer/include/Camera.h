#include <amp.h>
#include <amp_math.h>
#include "geometry/Vector.h"
#include "geometry/Point.h"

using namespace concurrency;
using namespace concurrency::fast_math;
using namespace geometry;

#ifndef Camera_H
#define Camera_H

class Camera {

	public:

		Camera() restrict(amp, cpu) {
			up = Vector(0, 1, 0);
			eye = Point(0, 0, 0);
			center = Point(0, 0, 0);
		}

		Camera(Point cP, Vector upVec, Point eP, float fieldOfViewV) restrict(amp, cpu);

		void adjustCamera(int imageWidth, int imageHeight) restrict(amp, cpu) {

			aspectRatio = imageWidth / float(imageHeight);
			alpha = fieldOfView / 2.f;
			height = 2 * concurrency::fast_math::tan(alpha);
			width = aspectRatio * height;
			pixelWidth = width / (imageWidth - 1);
			pixelHeight = height / (imageHeight - 1);

		}

		Point eye, center;
		Vector f, s, u, up;
		float aspectRatio, alpha, height, width, pixelWidth, pixelHeight, fieldOfView;

		#ifdef GAALOP
		float camera_plane[4];
		#endif
};

#endif

