#include <amp.h>
#include "geometry/Vector.h"

using namespace concurrency;
using namespace geometry;

#ifndef Color_H
#define Color_H


class Color : public Vector {

	public:

		// constructors
		Color() restrict(amp, cpu) : Vector() {};
		Color(float r, float g, float b) restrict(amp, cpu) : Vector(r, g, b) {};

		// operators
		Color operator+(Color& right) restrict(amp, cpu);
		Color operator*(Color& right) restrict(amp, cpu) { return Color(x * right.x, y * right.y, z * right.z); }
		Color operator*(float right) restrict(amp, cpu) { return Color(x * right, y * right, z * right); }
		Color& operator+=(Color right) restrict(amp, cpu) {
				x += right.x;
				y += right.y;
				z += right.z;
				return *this;
		}

};

#endif
