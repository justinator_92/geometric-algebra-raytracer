#include "Color.h"
#include "geometry/Point.h"

using namespace geometry;

#ifndef Light_H
#define Light_H

class Light {

	public:

		Light(Point position, Color intensity) restrict(amp, cpu) {
			this->position = position;
			this->intensity = intensity;
		}

		Point position;
		Color intensity;
};

#endif
