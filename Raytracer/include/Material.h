#include <amp.h>
#include "Color.h"
#include "geometry/Point.h"

using namespace geometry;

#ifndef Material_H
#define Material_H

class Material {

	public:

		Material() restrict(amp, cpu) {

		}

		Material(Color ambientColor, float glossiness = 0.1f) restrict(amp, cpu);

		Color getColorAt(Point p);


		//Color renderColor(Ray lightRay, Vector normal, Color lightColor, Vector rayDirection) restrict(amp, cpu);

		Color ambientColor, diffuseColor, specularColor;
		float glossiness, n, specConst;
		Color baseLight, specularBase;
		Color AMBIENT_COLOR;

};

#endif
