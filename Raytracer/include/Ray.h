#include <amp.h>
#include "geometry/Point.h"

using namespace concurrency;
using namespace concurrency::fast_math;

#ifndef Ray_H
#define Ray_H


class Ray {

	public:

	Ray(Point origin, Vector direction) restrict(amp, cpu);
	
	Point pointAtParameter(float d) restrict(amp, cpu) {
		return origin + direction * d;
	}

	Point origin;
	Vector direction;

	#ifdef GAALOP
	float ray[6];
	float dist;
	float camera_plane[4];  // obsolete...?
	#endif
};

#endif


