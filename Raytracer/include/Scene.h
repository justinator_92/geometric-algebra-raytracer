#include <amp.h>
#include <amp_math.h>
#include <iostream>
#include <limits>
#include "Light.h"
#include "Color.h"
#include "Ray.h"
#include "Sphere.h"
#include "Camera.h"

using namespace std;
using namespace concurrency;
using namespace concurrency::fast_math;
using namespace geometry;

#ifndef Scene_H
#define Scene_H

class Scene {

	public:
		
		Scene(int width, int height, Camera camera) {
			this->width = width;
			this->height = height;		
			this->camera = camera;
			camera.adjustCamera(width, height);
			background = Color(0.f, 0.f, 0.f);
		}



		void addObject(Sphere obj) {
			objects.push_back(obj);
			cout << "Sphere added. Scene contains: " << objects.size() << " objects." << endl;
		}

		void renderOnGPU(vector<Color>& imageData, Light light);
		void renderOnCPU(vector<Color>& imageData, Light light);  

		Color background;

	protected:

		vector<Sphere> objects;
		Camera camera;
		int width, height;
		
		
};

#endif
