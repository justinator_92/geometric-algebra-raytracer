#include <amp.h>
#include <amp_math.h>
#include "geometry/Point.h"
#include "Material.h"
#include "Ray.h"
#include "utils/Intersection.h"


using namespace concurrency;
using namespace concurrency::fast_math;
using namespace geometry;

#ifndef Sphere_H
#define Sphere_H

class Sphere {

	public:

		Sphere(Point center, float rad, Material material);

		Vector normalAt(Point p) restrict(amp, cpu) {
			return (p - center).normalized();
		}

		bool operator == (const Sphere& obj) restrict(amp, cpu) {
			return center == obj.center;
		}


		bool intersectParameter(Ray ray, float point[]);
		float sqrtf(float d) restrict(amp, cpu) { return concurrency::fast_math::sqrtf(d); } // wrapper method for gaalop sqrtf...
		
		int radius;
		Point center;
		Material material;

		#ifdef GAALOP		
		float sphere[5];
		#endif

};

#endif

