#include <amp.h>
#include "Vector.h"

using namespace concurrency;

#ifndef Point_H
#define Point_H

namespace geometry {

	class Point {

		public:

			// constructors
			Point() restrict(amp, cpu) { this->x = 0; this->y = 0; this->z = 0; };
			Point(float x, float y, float z) restrict(amp, cpu) { this->x = x; this->y = y; this->z = z; };
		
			// operators
			Point operator+(const Vector& right) restrict(amp, cpu) {
				return Point(x + right.x, y + right.y, z + right.z);
			}

			Vector operator-(const Point& right) restrict(amp, cpu) {
				return Vector(x - right.x, y - right.y, z - right.z);
			}

			bool operator==(const Point& right) restrict(amp, cpu) {
				return (x == right.x && y == right.y && z == right.z);
			}

		//protected:
			// properties
			float x, y, z;		
	};

}

#endif
