#include <amp.h>
#include <amp_math.h>

using namespace concurrency;
using namespace concurrency::fast_math;

#ifndef Vector_H
#define Vector_H

namespace geometry {

	class Vector {

		public:
		
			// functions
			/*const float length() const restrict(amp, cpu);
			Vector normalized() const restrict(amp, cpu);
			float dot(Vector right) restrict(amp, cpu);
			Vector cross(Vector right) restrict(amp, cpu);
			Vector reflect(Vector normal) restrict(amp, cpu);*/

			// operators
			/*Vector operator-(const Vector& right) restrict(amp, cpu);

		
			Vector operator/(float right) restrict(amp, cpu);
			Vector operator*(float right) restrict(amp, cpu);
			Vector operator*(Vector& right) restrict(amp, cpu);
			friend Vector operator*(float left, Vector& right) restrict(amp, cpu);
			bool operator==(const Vector& right) restrict(amp, cpu);
			Vector& operator+=(Vector& right) restrict(amp, cpu);*/

			// constructors

			Vector() restrict(amp, cpu) { this->x = 0.f; this->y = 0.f; this->z = 0.f; }
			Vector(float x, float y, float z) restrict(amp, cpu) { this->x = x; this->y = y; this->z = z; }

			Vector scale(float factor) restrict(amp, cpu) { return Vector(x * factor, y * factor, z * factor); }
		

			Vector operator+(const Vector& right) restrict(amp, cpu) { return Vector(x + right.x, y + right.y, z + right.z); }
			Vector operator*(float right) restrict(amp, cpu) { return Vector(x * right, y * right, z * right); }

			Vector operator-(const Vector& right) restrict(amp, cpu) {
				return Vector(x - right.x, y - right.y, z - right.z);
			}

			Vector normalized() restrict(amp, cpu) {
				float norm = length();
				Vector ret;
				if (norm != 0){
					ret.x = x / norm;
					ret.y = y / norm;
					ret.z = z / norm;
				}
				return ret;
			}

			void normalize() restrict(amp, cpu) {
				float norm = length();
				x /= norm;
				y /= norm;
				z /= norm;
			}

			float length() restrict(amp, cpu) { return concurrency::fast_math::sqrt(x * x + y * y + z * z); }

			float dot(Vector right) restrict(amp, cpu) { return x * right.x + y * right.y + z * right.z; }

			Vector cross(Vector right) restrict(amp, cpu) {
				return Vector(y * right.z - z * right.y,
						z * right.x - x * right.z,
						x * right.y - y * right.x);
			}

			Vector reflect(Vector normal) restrict(amp, cpu) {
				Vector self(x, y, z);
				return self - normal * normal.dot(self) * 2.f;
			}

			bool operator==(const Vector& right) restrict(amp, cpu) {
				return (x == right.x && y == right.y && z == right.z);
			}

			Vector& operator+=(Vector& right) restrict(amp, cpu) {
				x += right.x;
				y += right.y;
				z += right.z;
				return *this;
			}

		/*	Vector Vector::operator/(float right) restrict(amp, cpu) {
				return Vector(x / right, y / right, z / right);
			}*/

	/*
			Vector Vector::operator-(const Vector& right) restrict(amp, cpu) {
				return Vector(x - right.x, y - right.y, z - right.z);
			}

			


			Vector Vector::operator/(float right) restrict(amp, cpu) {
				return Vector(x / right, y / right, z / right);
			}

		

			Vector Vector::operator*(Vector& right) restrict(amp, cpu) {
				return Vector(x * right.x, y * right.y, z * right.z);
			}

			Vector operator*(float left, Vector& right) restrict(amp, cpu) {
				return Vector(left * right.x, left * right.y, left * right.z);
			}

			bool Vector::operator==(const Vector& right) restrict(amp, cpu) {
				return (x == right.x && y == right.y && z == right.z);
			}

			Vector& Vector::operator+=(Vector& right) restrict(amp, cpu) {
				x += right.x;
				y += right.y;
				z += right.z;
				return *this;
			}
	*/
			// properties
			float x, y, z;
	};

}

#endif
