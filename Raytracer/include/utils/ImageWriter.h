#include <fstream>
#include <string>
#include <iostream>


#include "../Color.h"

using namespace std;

#ifndef ImageWriter_H
#define ImageWriter_H

class ImageWriter {

	public:

		static void SaveImageAsPPM(string filename, std::vector<Color>& imageData, int imageWidth, int imageHeight) {

			string file = filename + ".ppm";

			fstream ppmFileStream(file, ios_base::out | ios_base::trunc);

			ppmFileStream << "P3" << endl << imageWidth << endl << imageHeight << endl << 255 << endl;	
			for (int y = 0; y < imageHeight; ++y) {	
				for (int x = 0; x < imageWidth; ++x) {
					Color color(imageData[y * imageWidth + x]);
					ppmFileStream << (color.x > 1 ? 255 : int(color.x * 255)) << " " << 
							 (color.y > 1 ? 255 : int(color.y * 255)) << " " << 
							 (color.z > 1 ? 255 : int(color.z * 255)) << " ";
				}
				ppmFileStream << endl;
			}

			cout << "Image: " << file << " written." << endl;

		}
};


#endif
