#include <amp.h>
#include <amp_math.h>


#ifndef Intersection_H
#define Intersection_H

class Intersection {

	public:
		
		Intersection() restrict(amp, cpu) {
			this->hasIntersection = false;
			this->distance = 0.f;
		}

		Intersection(bool hasIntersection, float distance) restrict(amp, cpu) {
			this->hasIntersection = hasIntersection;
			this->distance = distance;
		}

		bool hasIntersection;
		float distance;
		
};


#endif
