/**
 *
 * 	Geometric Algebra Raytracer using C++ AMP 
 * 	Application is running on HSA's C++ AMP Compiler for Linux
 * 
 * 	@Author: Justin Amadeus Albert 
 * 	University of Applied Sciences Wiesbaden
 * 
 * 	@Date: October 1st 2015
 *
 **/

#include <iostream>
#include <string>
#include "../include/utils/ImageWriter.h"
#include "../include/Scene.h"

using namespace std;

static const int sizeFactor = 8;  // for easy size change
static int WIDTH = int(320 * sizeFactor);
static int HEIGHT = int(240 * sizeFactor);

#ifdef GAALOP
string fileName = "_GA_rayTracer";
#else
string fileName = "_LA_rayTracer";
#endif

Light light(Point(2, 11, 6), Color(1, 1, 1));

enum RenderMode { GPU, CPU };

/**
 *	Renders Image specified by mode
 *	Return value is void
 */
void renderImage(Scene * scene, RenderMode mode) {

	string s = mode == GPU ? "GPU" : "CPU";

	std::vector<Color> imageData(WIDTH * HEIGHT);
	
	clock_t timeStamp = clock();

	if(mode == GPU) {	
		scene->renderOnGPU(imageData, light);
	} else {
		scene->renderOnCPU(imageData, light);
	}
	
	cout << "rendering on " << s << " took: " << float(clock() - timeStamp) / CLOCKS_PER_SEC << " sec. " << endl;

	ImageWriter::SaveImageAsPPM(s + fileName, imageData, WIDTH, HEIGHT);
	imageData.clear();  // put vector size to 0
}



int main(int argc, char * argv[]){
	
	#ifdef GAALOP
	cout << "USING GEOMETRIC ALGEBRA" << endl;
	#else
	cout << "USING LINEAR ALGEBRA" << endl;
	#endif

	Sphere sphere1 = Sphere(Point(2.5f, 3.f ,-10.f), 2.f, Material(Color(0.5f, 0.f, 0.f), 2));
	Sphere sphere2 = Sphere(Point(0.f, 7.f, -10.f), 2.f, Material(Color(0.f, 0.5f, 0.f), 3));
	Sphere sphere3 = Sphere(Point(-2.5f, 3.f, -10.f), 2.f, Material(Color(0.f, 0.f, 0.5f), 3));

	Camera camera = Camera(Point(0,3,0), Vector(0,1,0), Point(0,2,10),  45.f);
	camera.adjustCamera(WIDTH, HEIGHT);

	Scene * scene = new Scene(WIDTH, HEIGHT, camera);

	scene->addObject(sphere1);		
	scene->addObject(sphere2);		
	scene->addObject(sphere3);	 	
	
	renderImage(scene, GPU);
	renderImage(scene, CPU);
	
	return 0;
}
