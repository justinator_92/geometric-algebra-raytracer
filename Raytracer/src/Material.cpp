#include "../include/Material.h"

Material::Material(Color ambientColor, float glossiness) restrict(amp, cpu) {
	const float PI = 3.1452345f;
	this->ambientColor = ambientColor;
	this->diffuseColor = ambientColor;
	this->specularColor = Color(glossiness, glossiness, glossiness);

	this->glossiness = glossiness;
	this->n = 64.f * glossiness + 1;
	this->specConst = (n + 2.f) / (2.f * PI);
	this->AMBIENT_COLOR = Color(0.8f, 0.8f, 0.8f);

	this->baseLight = ambientColor * AMBIENT_COLOR;
	this->specularBase = specularColor * specConst;
	//cout << specularBase.x << ", " << baseLight.x << ", " << specularColor.x << endl;
}

Color Material::getColorAt(Point p) {
	return baseLight;
}


/*Color Material::renderColor(Ray lightRay, Vector normal, Color lightColor, Vector rayDirection) restrict(amp, cpu) {
	Color color(0, 0, 0);
	float diffuseFactor = lightRay.direction.dot(normal);

	if (diffuseFactor > 0){
		color += diffuseColor * lightColor * diffuseFactor;
		Vector reflection = lightRay.direction.reflect(normal).normalized();
		float specularFactor = reflection.dot(rayDirection);
		if (specularFactor > 0)
			color += specularBase * lightColor * pow(specularFactor, n);
	}

	return color;
}*/
