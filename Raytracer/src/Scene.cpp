#include "../include/Scene.h"

Ray::Ray(Point origin, Vector direction) restrict(amp, cpu) {
		this->origin = origin;
		this->direction = direction.normalized();	
}


Camera::Camera(Point cP, Vector upVec, Point eP, float fieldOfViewV) restrict(amp, cpu) {
	this->up = upVec;
	this->eye = eP;
	this->center = cP;

	// build camera coordsystem
	this->f = (center - eye).normalized();
	this->s = (f.cross(up)).normalized();
	this->u = (s.cross(f)).scale(-1);

	float PI = concurrency::fast_math::atan(1.0f) * 4.f;
	fieldOfView = fieldOfViewV * PI / 180.f;

	#ifdef GAALOP

	float Fx = f.x;
	float Fy = f.y;
	float Fz = f.z;
	float d = Fx * eye.x + Fy * eye.y + Fz * eye.z;

	#pragma gpc begin
	#pragma clucalc begin
	    ?Plane = Fx * e1 + Fy * e2 + Fz * e3 + d * einf;
	#pragma clucalc end
	    camera_plane = mv_to_array(Plane, e1, e2, e3, einf);
	#pragma gpc end

	#endif
}

#ifdef GAALOP
inline float calculateDistance(float p[], Ray ray) restrict(amp, cpu) {

	#pragma gpc begin
	    VPlane = mv_from_array(ray.camera_plane, e1, e2, e3, einf);
	    Point = mv_from_array(p, e1, e2, e3, einf, e0);
	#pragma clucalc begin
	    ?dist = Point . VPlane;
	#pragma clucalc end
	    float d = mv_get_bladecoeff(dist, 1);
	#pragma gpc end
	//cout << d << endl;
	return d;
	
}
#endif

Intersection Sphere::intersectParameter(Ray ray) restrict(amp, cpu) {

	Intersection ret(false, 0);

	#ifdef GAALOP

	float point[5];
	float camera_plane[4];
	camera_plane[0] = ray.camera_plane[0];
	camera_plane[1] = ray.camera_plane[1];
	camera_plane[2] = ray.camera_plane[2];
	camera_plane[3] = ray.camera_plane[3];
	//std::copy(ray.camera_plane, ray.camera_plane+4, camera_plane); // TODO: needt this
		
	#pragma gpc begin
		    VPlane = mv_from_array(camera_plane, e1, e2, e3, einf);
		    Sphere = mv_from_array(sphere, e1, e2, e3, einf, e0);
		    Ray = mv_from_array(ray.ray, e1^e2, e1^e3, e1^einf, e2^e3, e2^einf, e3^einf);
		#pragma clucalc begin
		    ?PP = *(Ray ^ Sphere);
		    ?hasIntersection = PP . PP;
		#pragma clucalc end
			//cout << hasIntersection << endl;
		    if (hasIntersection <= 0)
			return ret;

		#pragma clucalc begin
		    denom = e0 . PP;
		    P1 = (sqrt(PP . PP) + PP) / denom;
		    ?P1normed = P1 / (-P1.einf); // scalar value of e0
		    P2 = (-sqrt(PP . PP) + PP) / denom;
		    ?P2normed = P2 / (-P2.einf); // scalar value of e0
		    ?d1 = P1normed . VPlane; // TODO fixme
		    ?d2 = P2normed . VPlane;
		#pragma clucalc end
		    float t1 = mv_get_bladecoeff(d1, 1);
		    float t2 = mv_get_bladecoeff(d2, 1);
		    bool P1V = t1 > 0;
		    bool P2V = t2 > 0;
		    if (!P1V && !P2V)
			return ret;
		   
		    if (P1V && !P2V)
		    {
			point = mv_to_array(P1normed, e1, e2, e3, einf, e0);
			return Intersection(true, calculateDistance(point, ray));
		    }
		    if (P2V && !P1V)
		    {
			point = mv_to_array(P2normed, e1, e2, e3, einf, e0);
			return Intersection(true, calculateDistance(point, ray));
		    }
		    if (P1V && P2V)
		    {
			if (t1 < t2)
			{
			    point = mv_to_array(P1normed, e1, e2, e3, einf, e0);
			}
			else
			{
			    point = mv_to_array(P2normed, e1, e2, e3, einf, e0);
			}
			return Intersection(true, calculateDistance(point, ray));
		    }
		#pragma gpc end
	
	return ret;

	#else

	Vector co = center - ray.origin;
	float v = co.dot(ray.direction);
	float discriminant = (radius * radius) - (co.dot(co) - v * v);
	if (discriminant < 0)
		return ret;
	return Intersection(true, v - concurrency::fast_math::sqrt(discriminant));

	#endif
}




inline Ray createRay(Camera camera, int x, int y) restrict(amp, cpu) {

	Vector xComp = camera.s.scale((x * camera.pixelWidth - camera.width / 2.f));
	Vector yComp = camera.u.scale((y * camera.pixelHeight - camera.height / 2.f));
	Ray newRay(camera.eye, camera.f + xComp + yComp);

	#ifdef GAALOP

	Vector direction = (camera.f + xComp + yComp).normalized();

	float Ox = camera.center.x;
	float Oy = camera.center.y;
	float Oz = camera.center.z;

	float Lx = camera.center.x + direction.x;
	float Ly = camera.center.y + direction.y;
	float Lz = camera.center.z + direction.z;

	#pragma gpc begin
	#pragma clucalc begin
		O = VecN3(Ox, Oy, Oz);
		L = VecN3(Lx, Ly, Lz);	
		?Ray = *(O ^ L ^ einf);
	#pragma clucalc end  
		newRay.ray = mv_to_array(Ray, e1^e2, e1^e3, e1^einf, e2^e3, e2^einf, e3^einf);
	#pragma gpc end	

	//std::copy(camera.camera_plane, camera.camera_plane+4, newRay.camera_plane); TODO: Need this
	newRay.camera_plane[0] = camera.camera_plane[0];
	newRay.camera_plane[1] = camera.camera_plane[1];
	newRay.camera_plane[2] = camera.camera_plane[2];
	newRay.camera_plane[3] = camera.camera_plane[3];

	#endif

	return newRay;
}


// inline bool inShadow(Ray ray, array_view<Sphere, 1> objects, Sphere ignoredObject, int length) restrict(amp, cpu) {

// 	for (int i = 0; i < length; i++) {
// 		Sphere obj = objects[i];

// 		if (obj == ignoredObject)
// 			continue;
		
// 		//Intersection inter = obj.intersectParameter(ray);

// 		Intersection inter(false, 0);
// 		if(inter.hasIntersection)
// 			return true;
// 	}
// 	return false;
// }


inline void intersectObjects(Ray ray, int& objectIndex, float& distance, array_view<Sphere, 1> objects, int length) restrict(amp, cpu) {

	float hitDistance;
	Intersection intersection;

	for (int i = 0; i < length; i++) {
		
		intersection = objects[i].intersectParameter(ray);
		
		if(!intersection.hasIntersection)
			continue;		

		hitDistance = intersection.distance;

		if (hitDistance < distance && hitDistance > 0.001f)	{
			distance = hitDistance;
			objectIndex = i;
		}
	}
}


// inline Color renderColor(Ray reflector, Vector normal, Sphere object, Vector direction, Color color) restrict(amp, cpu) {

// 	float diffuseFactor = reflector.direction.dot(normal);

// 	if (diffuseFactor > 0){

// 		color += (object.material.diffuseColor * color * diffuseFactor);
// 		Vector reflection = reflector.direction.reflect(normal).normalized();
// 		float specularFactor = reflection.dot(direction);

// 		if (specularFactor > 0)
// 			color += object.material.specularBase * color * concurrency::fast_math::pow(specularFactor, object.material.n);
// 	}

// 	return color;
// 	// return Color(0.f, 0.f, 0.f);
// }

// inline Color calculateColor(Sphere object, array_view<Sphere, 1> objects, Light light, const int objectCount, Ray lightRay, Vector normal, Vector direction) restrict(amp, cpu) {

// 	Color color = object.material.ambientColor;

// 	if (inShadow(lightRay, objects, object, objectCount))
// 		return color;

// 	return renderColor(lightRay, normal, object, direction, color);
// 	//return Color(0.f, 0.f, 0.f);
// }


Color rayCastAlgorithm(int x, int y, Ray ray, array_view<Sphere, 1> objects, Light light, const int objectCount, Color background) restrict(amp, cpu) {

	float distance;
	int objectIndex;

	Color color = background;
	bool inShadow = false;

	for(int level = 2; level > 0; level--) {

		distance = 1000000000;  // TODO: add positive infinity
		objectIndex = -1;

		intersectObjects(ray, objectIndex, distance, objects, objectCount);

		if (objectIndex < 0) 
			return color;
		
		

		Sphere object = objects[objectIndex];
		Point intersection = ray.pointAtParameter(distance);
		Vector normal = object.normalAt(intersection);
		
		Ray lightRay(intersection, light.position - intersection);

		// color += calculateColor(object, objects, light, objectCount, lightRay, normal, ray.direction);

		color += object.material.ambientColor;



		// check if object is in shadow...

		for (int i = 0; i < objectCount; i++) {
			Sphere obj = objects[i];

			if (obj == object)
				continue;
			
			Intersection inter = obj.intersectParameter(ray);

			if(inter.hasIntersection) {
				inShadow = true;
				break;
			}
		}
		
		if(inShadow)
			return color;  // if yes, return just ambient color...

		float diffuseFactor = lightRay.direction.dot(normal);

		if (diffuseFactor > 0){

			color += (object.material.diffuseColor * color * diffuseFactor);
			Vector reflection = lightRay.direction.reflect(normal).normalized();
			float specularFactor = reflection.dot(ray.direction);

			if (specularFactor > 0)
				color += object.material.specularBase * color * concurrency::fast_math::pow(specularFactor, object.material.n);
		}
	
		if(level == 0)
		 	return color;

		ray = Ray(intersection, ray.direction.reflect(normal));

	}

	return color;
}







void Scene::renderOnGPU(vector<Color>& imageData, Light light) {
	
	// pass normal vector data to amp wrapper object... tile object is necessary for synchronization...
	array_view<Color, 2> imageView(height, width, &imageData[0]);

	// put objects from array to AMP wrapper object...
	array_view<Sphere, 1> allObjects(objects.size(), objects);
	
	Camera cam = camera;
	Color back = background;

	int w = width;
	int h = height;

	int objectSize = objects.size();

	parallel_for_each(imageView.get_extent(), [=](index<2> idx)  restrict(amp)
	{
		const auto y = idx[0];  // inverse order...
		const auto x = idx[1];

		Camera cam = Camera(Point(0,3,0), Vector(0,1,0), Point(0,2,10), 45.f);
		cam.adjustCamera(w, h);
		Ray newRay = createRay(cam, x, y);
		
		Color col = rayCastAlgorithm(x, y, newRay, allObjects, light, objectSize, back);	
		imageView[idx] = col;
	});

	//imageView.synchronize();
}


void Scene::renderOnCPU(vector<Color>& imageData, Light light) {

	array_view<Sphere, 1> allObjects(objects.size(), objects);
	imageData.clear();  // put vector size to 0

	int objectSize = objects.size();

	Camera cam = camera;
	

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {

			Ray newRay = createRay(cam, x, y);
			Color col = rayCastAlgorithm(x, y, newRay, allObjects, light, objectSize, background);
			//cout << col.x << "," << col.y << "," << col.z << endl;
			imageData.push_back(col);
		}
	}
}
