#include "../include/Sphere.h"

Sphere::Sphere(Point center, float rad, Material material) {
	
	this->center = center;
	this->radius = rad;
	this->material = material;

	#ifdef GAALOP

	float Cx = center.x;
	float Cy = center.y;
	float Cz = center.z;

	#pragma gpc begin
	#pragma clucalc begin
		?S = *SphereN3(Cx, Cy, Cz, radius);
	#pragma clucalc end
    		sphere = mv_to_array(S, e1, e2, e3, einf, e0);
	#pragma gpc end

	#endif
}


